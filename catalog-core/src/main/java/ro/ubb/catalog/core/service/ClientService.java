package ro.ubb.catalog.core.service;

import org.springframework.data.domain.Sort;
import ro.ubb.catalog.core.model.Client;

import java.util.List;

public interface ClientService {

    List<Client> getAllClients();
    Client addClient(Client client);
    Client updateClient(Integer id, Client client);
    void deleteClient(Integer id);
    List<Client> getAllClients(Sort sort);

}
