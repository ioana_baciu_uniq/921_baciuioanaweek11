package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Purchase extends BaseEntity<Integer> {

    @Column
    private Integer bookID;
    @Column
    private Integer clientID;
    @Column
    private Date pdate;

}
