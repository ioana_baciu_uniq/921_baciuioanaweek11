package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Purchase;

import java.util.HashMap;
import java.util.List;

public interface PurchaseService {
    List<Purchase> getAllPurchases();
    Purchase addPurchase(Purchase purchase);
    void deletePurchase(Integer id);
    Book getBook(Integer id);
    Client getClient(Integer id);
    Purchase updatePurchase(Integer id, Purchase purchase);
    HashMap<Client, Double> getClientsMoney();

}
