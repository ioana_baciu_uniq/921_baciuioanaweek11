package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.repository.BookRepository;
import ro.ubb.catalog.core.repository.ClientRepository;
import ro.ubb.catalog.core.repository.PurchaseRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class PurchaseServiceImpl implements PurchaseService {
    public static final Logger logger = LoggerFactory.getLogger(PurchaseServiceImpl.class);

    @Autowired
    BookRepository bookRepository;
    @Autowired
    ClientRepository clientRepository;

    @Autowired
    PurchaseRepository purchaseRepository;

    @Override
    public List<Purchase> getAllPurchases() {
        logger.trace("getAllPurchases - entered");
        return purchaseRepository.findAll();
    }

    @Override
    public Purchase addPurchase(Purchase purchase) {
        logger.trace("addPurchase -- entered, p = {}", purchase);
        Purchase save = purchaseRepository.save(purchase);
        logger.trace("addPurchase -- finished, p = {}", save);
        return save;
    }

    @Override
    public void deletePurchase(Integer id) {
        logger.trace("deletePurchase -- entered, id = {}", id);
        purchaseRepository.deleteById(id);
        logger.trace("deletePurchase -- finished");
    }

    @Override
    @Transactional
    public Purchase updatePurchase(Integer id, Purchase purchase) {
        logger.trace("updatePurchase - method enered, id = {}, purchase = {}", id, purchase);
        Optional<Purchase> optional = purchaseRepository.findById(id);
        optional.ifPresent(p->{
            p.setBookID(purchase.getBookID());
            p.setClientID(purchase.getClientID());
            p.setPdate(purchase.getPdate());
        });
        logger.trace("updatePurchase - finished, purchase = {}", optional);

        return optional.orElse(null);

    }

    @Override
    public Book getBook(Integer id) {

        Optional<Book> book = this.bookRepository.findById(id);
        return book.orElse(null);
    }

    @Override
    public Client getClient(Integer id) {
        Optional<Client> client = this.clientRepository.findById(id);
        return client.orElse(null);
    }

    @Override
    public HashMap<Client, Double> getClientsMoney() {
        logger.trace("getClientsMoney ---- method entered");
        HashMap<Client, Double> result = new HashMap<>();
        Iterable<Client> clients = clientRepository.findAll();
        List<Purchase> purchases = purchaseRepository.findAll();
        clients.forEach(x -> { result.put(x, 0.0); });


        for (Purchase p: purchases) {
            Client client = clientRepository.findById(p.getClientID()).get();
            System.out.println(client);
            Double res = result.get(client) + bookRepository.findById(p.getBookID()).get().getPrice();
            result.put(client, res);
        }
        logger.trace("getClientsMoney ---- method finished, result:{}", result);

        return result;
    }
}
