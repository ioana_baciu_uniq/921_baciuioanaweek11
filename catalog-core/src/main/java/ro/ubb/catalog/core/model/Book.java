package ro.ubb.catalog.core.model;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Book extends BaseEntity<Integer>{

    private String IBAN;
    private String author;
    private String title;
    private Integer pageCount;//D: changed type from int to Integer
    private Double price;

}
