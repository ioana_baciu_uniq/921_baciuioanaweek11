package ro.ubb.catalog.core.service;

import org.springframework.data.domain.Sort;
import ro.ubb.catalog.core.model.Book;

import java.util.List;

public interface BookService {

    List<Book> getAllBooks();
    Book addBook(Book book);
    Book updateBook(Integer id, Book book);
    void deleteBook(Integer id);
    List<Book> getAllBooks(Sort sort);
    List<Book> filterBooksByPrice(Double price);


}
