package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.repository.ClientRepository;
import ro.ubb.catalog.core.repository.PurchaseRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {
    public static final Logger logger = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    PurchaseRepository purchaseRepository;

    @Override
    public List<Client> getAllClients() {
        logger.trace("getAllClients");
        return clientRepository.findAll();
    }

    @Override
    public Client addClient(Client client) {
        logger.trace("addClient ---- method entered");
        Client save = clientRepository.save(client);
        logger.trace("addClient ----- method finished, result = {}", client);
        return client;
    }

    @Override
    @Transactional
    public Client updateClient(Integer id, Client client) {

        Optional<Client> clientOptional = clientRepository.findById(id);
        clientOptional.ifPresent(c->{
            c.setFullName(client.getFullName());
            c.setAddress(client.getAddress());
        });

        return clientOptional.orElse(null);
    }

    @Override
    public void deleteClient(Integer id) {
        purchaseRepository.findAll().stream().filter(x->x.getClientID().equals(id)).map(x->x.getId()).forEach(x->purchaseRepository.deleteById(x));
        clientRepository.deleteById(id);
    }

    @Override
    public List<Client> getAllClients(Sort sort) {
        logger.trace("getAllClients ----- method entered");
        return clientRepository.findAll(sort);
    }
}
