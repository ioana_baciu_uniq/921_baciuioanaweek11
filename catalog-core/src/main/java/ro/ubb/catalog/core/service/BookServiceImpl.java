package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.repository.BookRepository;
import ro.ubb.catalog.core.repository.PurchaseRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {
    public static final Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);
    //todo log
    @Autowired
    BookRepository bookRepository;
    @Autowired
    PurchaseRepository purchaseRepository;

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Book addBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    @Transactional
    public Book updateBook(Integer id, Book book) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        optionalBook.ifPresent(b->{
            b.setTitle(book.getTitle());
            b.setPrice(book.getPrice());
            b.setPageCount(book.getPageCount());
            b.setIBAN(book.getIBAN());
            b.setAuthor(book.getAuthor());
                }

        );
        return optionalBook.orElse(null);
    }

    @Override
    public void deleteBook(Integer id) {

        purchaseRepository.findAll().stream().filter(x->x.getBookID().equals(id)).map(x->x.getId()).forEach(x->purchaseRepository.deleteById(x));
        bookRepository.deleteById(id);
    }

    @Override
    public List<Book> getAllBooks(Sort sort) {
        logger.trace("getAllBooks ---- method entered");
        return bookRepository.findAll(sort);

    }
    @Override
    public List<Book> filterBooksByPrice(Double price) {

        //D: am spus sa fiu opulenta si sa te las sa mai lucrezi la html.. And maybe we need it?
        List<Book> all = bookRepository.findAll();
        List<Book> filteredBooks = new ArrayList<>();
        all.forEach(filteredBooks::add);
        //filteredBooks.removeIf(book -> !(book.getPrice() == price));
        return filteredBooks.stream().filter(x->x.getPrice()>price).collect(Collectors.toList());

    }
}
