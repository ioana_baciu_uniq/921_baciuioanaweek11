package ro.ubb.catalog.web.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.service.PurchaseService;
import ro.ubb.catalog.web.dto.PurchaseDto;

@Component
public class PurchaseConverter extends BaseConverter<Purchase, PurchaseDto> {

    @Autowired
    PurchaseService purchaseService;



    @Override
    public Purchase convertDtoToModel(PurchaseDto dto) {
        Purchase purchase = Purchase.builder().bookID(dto.getBook().getId()).clientID(dto.getClient().getId()).pdate(dto.getPdate()).build();
        purchase.setId(dto.getId());
        return purchase;
    }

    @Override
    public PurchaseDto convertModelToDto(Purchase purchase) {
        Book book = purchaseService.getBook(purchase.getBookID());
        Client client = purchaseService.getClient(purchase.getClientID());

        PurchaseDto purchaseDto = PurchaseDto.builder().book(book).client(client).pdate(purchase.getPdate()).build();
        purchaseDto.setId(purchase.getId());
        return purchaseDto;

        }

}
