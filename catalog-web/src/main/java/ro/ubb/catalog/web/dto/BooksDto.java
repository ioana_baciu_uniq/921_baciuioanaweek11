package ro.ubb.catalog.web.dto;

import lombok.*;

import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BooksDto {
    private List<BookDto> books;

}
