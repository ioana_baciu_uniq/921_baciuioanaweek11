package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.web.dto.BookDto;

@Component
public class BookConverter extends BaseConverter<Book, BookDto> {
    @Override
    public Book convertDtoToModel(BookDto dto) {
        Book book = Book.builder()
                .IBAN(dto.getIBAN()).author(dto.getAuthor()).title(dto.getTitle()).pageCount(dto.getPageCount()).price(dto.getPrice()).build();
        book.setId(dto.getId());
        return book;
    }

    @Override
    public BookDto convertModelToDto(Book book) {
        BookDto b = BookDto.builder()
                .IBAN(book.getIBAN()).author(book.getAuthor()).title(book.getTitle()).pageCount(book.getPageCount()).price(book.getPrice()).build();
        b.setId(book.getId());
        return b;
    }
}
