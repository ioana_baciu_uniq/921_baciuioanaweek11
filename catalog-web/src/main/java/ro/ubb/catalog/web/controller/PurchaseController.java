package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.service.PurchaseService;
import ro.ubb.catalog.web.converter.ClientConverter;
import ro.ubb.catalog.web.converter.PurchaseConverter;
import ro.ubb.catalog.web.dto.BookDto;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.PurchaseDto;

import java.util.ArrayList;
import java.util.HashMap;

@RestController
public class PurchaseController {

    public static final Logger logger = LoggerFactory.getLogger(PurchaseController.class);
    @Autowired
    PurchaseService purchaseService;
    @Autowired
    PurchaseConverter purchaseConverter;

    @Autowired
    ClientConverter clientConverter;

    @RequestMapping(value = "/purchases", method = RequestMethod.GET)
    ArrayList<PurchaseDto> getAllPurchases()
    {
        logger.trace("getAllPurchases");
        return new ArrayList<>(purchaseConverter.convertModelsToDtos(purchaseService.getAllPurchases()));
    }

    @RequestMapping(value = "/purchases", method = RequestMethod.POST)
    PurchaseDto addPurchase(@RequestBody PurchaseDto purchaseDto)
    {
        logger.trace("addPurchase ----- entered");
        PurchaseDto purchase = purchaseConverter.convertModelToDto(purchaseService.addPurchase(purchaseConverter.convertDtoToModel(purchaseDto)));
        logger.trace("addPurchase ----- finished, result = {}",purchase);
        return purchase;

    }

    @RequestMapping(value = "/purchases/{purchaseId}", method = RequestMethod.PUT)
    public PurchaseDto updatePurchase(
            @PathVariable final Integer purchaseId,
            @RequestBody final PurchaseDto purchaseDto) {
        logger.trace("updatePurchase: purchaseId={}, purchaseDtoMap={}", purchaseId, purchaseDto);

        PurchaseDto purchase = purchaseConverter.convertModelToDto(purchaseService.updatePurchase(purchaseId, purchaseConverter.convertDtoToModel(purchaseDto)));

        // StudentDto result = studentConverter.convertModelToDto(student);

        logger.trace("updatePurchase: result={}", purchase);

        return purchase;
    }

    @RequestMapping(value = "/purchases/{purchaseId}", method = RequestMethod.DELETE)
    ResponseEntity<?> deletePurchase(@PathVariable Integer purchaseId)
    {
        logger.trace("deletePurchase --- entered, id = {}", purchaseId);
        purchaseService.deletePurchase(purchaseId);
        logger.trace("deletePurchase --- finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/purchases/map", method = RequestMethod.GET)
    HashMap<ClientDto, Double> getClientsMoney()
    {
        logger.trace("getClientsMoney ---- method entered");
        HashMap<Client, Double> map = purchaseService.getClientsMoney();
        HashMap<ClientDto, Double> anothermap = new HashMap<>();
        map.entrySet().forEach(x->anothermap.put(clientConverter.convertModelToDto(x.getKey()), x.getValue()));
        logger.trace("getClientsMoney ---- method finished");
        return anothermap;
    }
}
