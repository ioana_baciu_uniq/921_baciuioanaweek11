package ro.ubb.catalog.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.core.service.BookService;
import ro.ubb.catalog.web.converter.BookConverter;
import ro.ubb.catalog.web.dto.BookDto;
import ro.ubb.catalog.web.dto.BooksDto;
import ro.ubb.catalog.web.dto.StudentDto;

import java.net.http.HttpResponse;
import java.util.ArrayList;

@RestController
public class BookController {
    public static final Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    BookService bookService;
    @Autowired
    BookConverter bookConverter;

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    ArrayList<BookDto> getAllBooks()
    {
        logger.trace("getAllBooks");
        return new ArrayList<>(bookConverter.convertModelsToDtos(bookService.getAllBooks()));
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    BookDto addBook(@RequestBody BookDto bookDto)
    {
        logger.trace("addBook ----- entered");
        BookDto book = bookConverter.convertModelToDto(bookService.addBook(bookConverter.convertDtoToModel(bookDto)));
        logger.trace("addBook ----- finished, result = {}",book);
        return book;

    }

    @RequestMapping(value = "/books/{bookId}", method = RequestMethod.PUT)
    public BookDto updateBook(
            @PathVariable final Integer bookId,
            @RequestBody final BookDto bookDto) {
        logger.trace("updateBook: bookId={}, bookDtoMap={}", bookId, bookDto);

//        Student student = studentService.updateStudent(studentId,
//                studentDto.getSerialNumber(),
//                studentDto.getName(), studentDto.getGroupNumber());
        BookDto book = bookConverter.convertModelToDto(bookService.updateBook(bookId, bookConverter.convertDtoToModel(bookDto)));

       // StudentDto result = studentConverter.convertModelToDto(student);

        logger.trace("updateStudent: result={}", book);

        return book;
    }

    @RequestMapping(value = "/books/{bookId}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteBook(@PathVariable Integer bookId)
    {
        logger.trace("deleteBook --- entered, id = {}", bookId);
        bookService.deleteBook(bookId);
        logger.trace("deleteBook --- finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "books/sort/{sort}", method = RequestMethod.GET)
    ArrayList<BookDto> getBooksSorted(@PathVariable  String sort){
        logger.trace("getBooks ---> entered");
        //BooksDto books = new BooksDto(bookConverter.convertModelsToDtos(bookService.getAllBooks(Sort.by(sort))));
        ArrayList<BookDto> books = new ArrayList<>(bookConverter.convertModelsToDtos(bookService.getAllBooks(Sort.by(sort))));
        logger.trace("getBooks ; result: " + books);
        return books;

    }
    @RequestMapping(value = "/books/{price}", method = RequestMethod.GET)
        /*
         * La metoda aceasta am avut problema cu parametrul. Daca hardcodez variabila price cu o constanta, functioneaza bine
         *
         * */
    ArrayList<BookDto> filterBooksByPrice(@PathVariable Double price)
    {
        logger.trace("filterBooksByPrice ----> entered");
        ArrayList<BookDto> books = new ArrayList<>(bookConverter.convertModelsToDtos(bookService.filterBooksByPrice(price)));
        logger.trace("filterBooksByPrice ; result: " + books);
        return books;
    }
}
