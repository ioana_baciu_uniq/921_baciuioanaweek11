package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.web.converter.ClientConverter;
import ro.ubb.catalog.web.dto.BookDto;
import ro.ubb.catalog.web.dto.BooksDto;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.ClientsDto;

import java.util.ArrayList;

@RestController
public class ClientController {
    public static final Logger logger = LoggerFactory.getLogger(ClientController.class);
    @Autowired
    ClientService clientService;
    @Autowired
    ClientConverter clientConverter;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    ArrayList<ClientDto> getAllClients()
    {
        logger.trace("getAllClient");
        return new ArrayList<>(clientConverter.convertModelsToDtos(clientService.getAllClients()));
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto addClient(@RequestBody ClientDto clientDto)
    {
        logger.trace("addClient ----- entered");
        ClientDto client = clientConverter.convertModelToDto(clientService.addClient(clientConverter.convertDtoToModel(clientDto)));
        logger.trace("addClient ----- finished, result = {}",client);
        return client;

    }

    @RequestMapping(value = "/clients/{clientId}", method = RequestMethod.PUT)
    public ClientDto updateClient(
            @PathVariable final Integer clientId,
            @RequestBody final ClientDto clientDto) {
        logger.trace("updateClient: clientId={}, clientDtoMap={}", clientId, clientDto);
        ClientDto client = clientConverter.convertModelToDto(clientService.updateClient(clientId, clientConverter.convertDtoToModel(clientDto)));
        // StudentDto result = studentConverter.convertModelToDto(student);

        logger.trace("updateClient: result={}", client);

        return client;
    }

    @RequestMapping(value = "/clients/{clientId}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteClient(@PathVariable Integer clientId)
    {
        logger.trace("deleteClient --- entered, id = {}", clientId);
        clientService.deleteClient(clientId);
        logger.trace("deleteClient --- finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = "clients/sort/{sort}", method = RequestMethod.GET)
    ClientsDto getClientsSorted(@PathVariable String sort)
    {
        logger.trace("getClientsSorted ---- method entered");
        ClientsDto clientsDto = new ClientsDto(clientConverter.convertModelsToDtos(clientService.getAllClients(Sort.by(sort))));
        logger.trace("getClients ------- method finished");
        return clientsDto;
    }

}
