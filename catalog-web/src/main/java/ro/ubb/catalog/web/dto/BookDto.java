package ro.ubb.catalog.web.dto;


import lombok.*;

import javax.persistence.Column;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString(callSuper = true)
public class BookDto extends BaseDto {


    private String IBAN;
    private String author;
    private String title;
    private Integer pageCount;
    private Double price;
}
