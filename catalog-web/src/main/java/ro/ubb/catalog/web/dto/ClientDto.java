package ro.ubb.catalog.web.dto;


import lombok.*;
import org.springframework.context.annotation.Bean;

import javax.persistence.Column;

@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@Builder
@Getter
@Setter
public class ClientDto extends BaseDto {
    private String fullName;
    private String address;
}
